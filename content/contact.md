+++
title = "Contact"
slug = "contact"
+++

# Contact me

<form action="https://getform.io/f/ed8a8b41-f77e-43de-ac3c-37d254c360bc" method="POST">

  <input type="text" name="name">
  <input type="email" name="email">
  <input type="tel" name="tel">
  <button type="submit">Send</button>

</form>
        
# Get In Touch

Let's talk about your project and how I can help you or if you just wanna chat about ideas on science shoot me a message at bevanstanely at iisc.ac.in

1st  Year Student, Integrated PhD Biological Sciences, Division of Biological Sciences, Indian Institute of Science, Bengaluru, Karnataka - 560022
