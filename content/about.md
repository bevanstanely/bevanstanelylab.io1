+++
title = "About"
slug = "about"
+++

I am a student of biology with a wide variety of interests.
# Present Designation

Student of Integrated PhD in Biological Sciences at the Indian Institute of Science, Bengaluru.

# Interestss

- Emergent properties of a system
- Polycystic Ovarian Syndrome and Metabolic Syndrome
- Evolution and mechanisms of multi-cellularity
- Theoretical aspects of how cells can maintain heterogeneity
- Looking at cells stripped out of their biological context in a more conceptual framework
